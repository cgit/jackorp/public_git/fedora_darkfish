# Fedora Darkfish generator for RDoc

RDoc plugin for Fedora leveraging the Darkfish template that symlinks common template files.

## Installation

Add this line to your application's `Gemfile`:

```
gem 'fedora_darkfish'
```

And then execute the `bundle` command to install the gem.

Alternatively, you can also manually install the gem using the following command:

```
$ gem install fedora_darkfish
```

To use the gem with the `rdoc` command on CLI, you have to require it, so that RDoc is able to pick the gem up:
```
$ RUBYOPT='-r fedora_darkfish' rdoc --fmt=fedora
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
