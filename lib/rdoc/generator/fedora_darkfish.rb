require 'rdoc'
require 'rdoc/generator/darkfish'
require 'rdoc/generator/json_index'


class RDoc::Generator::Fedora < RDoc::Generator::Darkfish
  RDoc::RDoc.add_generator self

  def initialize store, options
    super store, options
    @json_index = RDoc::Generator::FedoraJsonIndex.new self,options
  end

  def install_rdoc_static_file source, destination, options # :nodoc:
    return unless source.exist?

    return if source.fnmatch?('*fonts/*')

    begin
      FileUtils.mkdir_p File.dirname(destination), **options
      FileUtils.ln_sf source, destination, **options
    rescue
    end
  end
end

class RDoc::Generator::FedoraJsonIndex < RDoc::Generator::JsonIndex
  def generate
    super

    out_dir = @base_dir + @options.op_dir

    Dir.chdir @template_dir do
      Dir['**/*.js'].each do |source|
        dest = File.join out_dir, source
        source = File.realpath source
        FileUtils.ln_sf source, dest, :verbose => $DEBUG_RDOC
      end
    end
  end

  # Don't generate gzipped content. Because we have always shipped also the
  # original, it just increases the size.
  def generate_gzipped
  end
end
