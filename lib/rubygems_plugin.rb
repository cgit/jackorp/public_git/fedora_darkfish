[:pre_install, :pre_reset, :pre_uninstall, :post_build, :post_install, :post_reset, :post_uninstall].each do |meth|
  block = Proc.new { puts 'This is ' + meth.to_s }
  Gem.send(meth, &block)
end

# class RDoc::RubygemsHook::Fedora < Rdoc::RubygemsHook
#   def document options, destination
#     
#   end
# end
Gem.pre_install do
  module RDoc
    class RubygemsHook
      alias generate_default generate

      def self.load_rdoc
        return if @fedora_darkfish_version

        require_relative 'rdoc/generator/fedora_darkfish'

        @fedora_darkfish_version = Gem::Version.new '1.0.0'
      end

      # def generate
      #   if Gem.rpmbuild?
      #     generate_fedora
      #   else
      #     generate_default
      #   end
      # end

      def generate #_fedora
        # Unfortunately the generator option for RDoc is not parametrized so
        # this has been Copied from RDoc::RubygemsHook#generate
        # and darkfish was replaced with fedora at the end of this method.
        return if @spec.default_gem?
        return unless @generate_ri or @generate_rdoc

        setup

        options = nil

        args = @spec.rdoc_options
        args.concat @spec.source_paths
        args.concat @spec.extra_rdoc_files

        case config_args = Gem.configuration[:rdoc]
        when String then
          args = args.concat config_args.split(' ')
        when Array then
          args = args.concat config_args
        end

        delete_legacy_args args

        Dir.chdir @spec.full_gem_path do
          options = ::RDoc::Options.new
          options.default_title = "#{@spec.full_name} Documentation"
          options.parse args
        end

        options.quiet = !Gem.configuration.really_verbose

        @rdoc = new_rdoc
        @rdoc.options = options

        store = ::RDoc::Store.new
        store.encoding = options.encoding
        store.dry_run  = options.dry_run
        store.main     = options.main_page
        store.title    = options.title

        @rdoc.store = store

        say "Parsing documentation for #{@spec.full_name}"

        Dir.chdir @spec.full_gem_path do
          @rdoc.parse_files options.files
        end

        document 'ri',       options, @ri_dir if
        @generate_ri   and (@force or not File.exist? @ri_dir)

        document 'fedora', options, @rdoc_dir if
        @generate_rdoc and (@force or not File.exist? @rdoc_dir)
      end
    end
  end
end

# Gem.post_install do
#   puts require 'fedora_darkfish'
# end
