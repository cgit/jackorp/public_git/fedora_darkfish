Gem::Specification.new do |spec|
  spec.name          = 'fedora_darkfish'
  spec.version       = '1.0.2'
  spec.authors       = ['Jaroslav Prokop']
  spec.email         = ['jar.prokop@volny.cz']

  spec.summary       = 'RDoc plugin for Fedora leveraging the Darkfish template'
  spec.description   = 'RDoc plugin for Fedora leveraging the Darkfish template' \
    'that symlinks common files.'
  spec.homepage      = 'https://fedorapeople.org/cgit/jackorp/public_git/fedora_darkfish.git/commit/?h=rawhide'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir["README.md", "LICENSE", "lib/**/*.rb"]
  spec.test_files = `git ls-files -- {test,spec,features}/*`.split("\n")

  spec.add_dependency 'rdoc'

  spec.add_development_dependency 'rake', '>= 12'
  spec.add_development_dependency 'test_unit'
end
