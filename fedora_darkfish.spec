# Generated from fedora_darkfish-1.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name fedora_darkfish

Name: rubygem-%{gem_name}
Version: 1.0.2
Release: 1%{?dist}
Summary: RDoc plugin for Fedora leveraging the Darkfish template
License: MIT
URL: https://gitlab.com/jackorp/fedora_darkfish
Source0: %{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(test_unit)
BuildArch: noarch

%description
RDoc plugin for Fedora leveraging the Darkfish templatethat symlinks common
files.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
# Run the test suite.
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%{gem_plugin}
%{gem_spec}
%exclude %{gem_cache}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md

%changelog
* Tue Oct 25 2022 Jarek Prokop <jprokop@redhat.com> - 1.0.0-1
- Initial package
